Documentation for Sky-City's query builder class.

## CQuery
	Class used to easily build up and perform database queries.

### Method: constructor( pointer Database )
  Initializes a query builder and queries the given database pointer.
### Method: Where( ... )
  Adds a where clause to the query.
  Accepts raw string, table or 2 arguments as fieldname and value with optional operator.
  Possible parameters:
  	Field and value: Where( fieldname, value )
  	Field and value with operator: Where( fieldname, value, operator )
  	Table: Where( { "fieldName": value } )
### Method: OrderBy( fieldName, orderbyDirection = "DESC" )
  Adds an order by clause to the query.
  To randomly order the results use "RAND()" as fieldname.
  Second parameter defaults to DESC and is not needed when sorting results randomly.
### Method: Join( tableName, joinCondition, joinType = null )
  This method adds a join statement to the query.
### Method: Duplicate( ... )
  Method used to update values of specific fields on a duplicate key entry in INSERT queries.
  Accepts raw string, table or 2 arguments as fieldname and value.
  Possible parameters:
  	Field and value: Duplicate( fieldname, value )
  	Field and value with operator: Duplicate( fieldname, value, operator )
  	Table: Duplicate( { "fieldName": value } )
### Method: Get( string Table, integer Limit = null, string Columns = "*" )
  Queries the specified table.
  Optional parameters are the limit of rows returned (specify null to return all rows)
  and the list of columns being returned.
### Method: GetOne( string Table, string Columns = "*" )
  Queries the specified table and returns only a single row.
  Optionally you can specify a list of columns returned.
### Method: Delete( string Table, integer Limit = null )
  Execute a delete query. Don't forget to add where clauses first.
### Method: Update( string Table, table|string Data )
  Method to update data in a table. Second parameter should be a table or raw string of field=>value items.
### Method: Insert( string Table, table Data )
  Method to insert data in a table. Second parameter should be a table of field=>value items.
### Method: InsertDelayed( string Table, table Data )
  Similar to Insert(), but does not wait for server response.
### Method: GetQuery( bool Build = false )
  Returns the performed query.
  The optional boolean parameter regenerates the query if set to true.
### Method: Clear()
  Resets all query data.
