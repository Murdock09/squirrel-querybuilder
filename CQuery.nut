/**
* Documentation for Sky-City's query builder class.
*
* CQuery
* 	Class used to easily build up and perform database queries.
*
*	Method: constructor( pointer Database )
*			Initializes a query builder and queries the given database pointer.
*	Method: Where( ... )
*			Adds a where clause to the query.
*			Accepts raw string, table or 2 arguments as fieldname and value with optional operator.
*			Possible parameters:
*				Field and value: Where( fieldname, value )
*				Field and value with operator: Where( fieldname, value, operator )
*				Table: Where( { "fieldName": value } )
*	Method: OrderBy( fieldName, orderbyDirection = "DESC" )
*			Adds an order by clause to the query.
*			To randomly order the results use "RAND()" as fieldname.
*			Second parameter defaults to DESC and is not needed when sorting results randomly.
*	Method: Join( tableName, joinCondition, joinType = null )
*			This method adds a join statement to the query.
*	Method: Duplicate( ... )
*			Method used to update values of specific fields on a duplicate key entry in INSERT queries.
*			Accepts raw string, table or 2 arguments as fieldname and value.
*			Possible parameters:
*				Field and value: Duplicate( fieldname, value )
*				Field and value with operator: Duplicate( fieldname, value, operator )
*				Table: Duplicate( { "fieldName": value } )
*	Method: Get( string Table, integer Limit = null, string Columns = "*" )
*			Queries the specified table.
*			Optional parameters are the limit of rows returned (specify null to return all rows)
*			and the list of columns being returned.
*	Method: GetOne( string Table, string Columns = "*" )
*			Queries the specified table and returns only a single row.
*			Optionally you can specify a list of columns returned.
*	Method: Delete( string Table, integer Limit = null )
*			Execute a delete query. Don't forget to add where clauses first.
*	Method: Update( string Table, table|string Data )
*			Method to update data in a table. Second parameter should be a table or raw string of field=>value items.
*	Method: Insert( string Table, table Data )
*			Method to insert data in a table. Second parameter should be a table of field=>value items.
*	Method: InsertDelayed( string Table, table Data )
*			Similar to Insert(), but does not wait for server response.
*	Method: GetQuery( bool Build = false )
*			Returns the performed query.
*			The optional boolean parameter regenerates the query if set to true.
*	Method: Clear()
*			Resets all query data.
*
* ===
*/

class CQuery
{
	_pDatabase	= null;
	_query		= "";	// The SQL query to be prepared and executed
	_table		= "";	// Table to query
	_columns	= "*";	// Columns to return
	_limit		= null;	// Limit of rows to return
	_updatestr	= "";	// Raw update string
	_where		= null;	// Array that holds where clauses
	_orderBy	= null;	// Array that holds order by clauses
	_join		= null;	// Array that holds joins
	_duplicate	= null;	// Array that holds values of duplicate key entries
	_update		= null;	// Table that holds update data
	_insert		= null;	// Table that holds insert data
	
	function _typeof()
	{
		return "CQuery";
	}
	
	constructor( pDatabase )
	{
		this._pDatabase = pDatabase;
		this._where		= [];
		this._orderBy	= [];
		this._join		= [];
		this._duplicate	= [];
		this._update	= {};
		this._insert	= {};
	}
	
	/**
		Function to format values depending on type to add to the query
		Ie. adding neccesary quotes and converting null to "IS NULL"
		
		@param String Field name
		@param String Field value
		@param String Operator
		@return String
	*/
	function _format( field, value, operator = "=" )
	{
		local valueType = typeof( value );
		
		// NULL values
		if ( valueType == "null" )
		{
			if ( operator == "=" )
				return ::format( "%s IS NULL", field );
			else
				return ::format( "%s IS NOT NULL", field );
		}
		
		// Other value types
		switch( valueType )
		{
			case "integer":
				return ::format( "%s %s %d", field, operator, value );
			case "float":
				return ::format( "%s %s %f", field, operator, value );
			default:
				return ::format( "%s %s '%s'", field, operator, ::Escape( value ) );
		}
	}
	
	function _formatUpdate( field, value )
	{
		local valueType = typeof( value );
		
		// NULL values
		if ( valueType == "null" )
			return ::format( "%s = NULL", field );
		else
			return this._format( field, value, "=" );
	}
	
	function _formatInsert( value )
	{
		local valueType = typeof( value );
		
		switch( valueType )
		{
			case "null":
				return "NULL";
			case "integer":
				return ::format( "%d", value );
			case "float":
				return ::format( "%f", value );
			default:
				return ::format( "'%s'", ::Escape( value ) );
		}
	}
	
	/**
		Method to add where clauses to the query.
		Accepts raw string, table or 2 arguments as fieldname and value.
		When given 2 arguments, an optional 3rd parameter can be used to change default operator "="
		
		Example:
			q.Where( "level", 1 );
			q.Where( "level", 1, ">=" );
			q.Where( "COUNT(*) = 1" );
			q.Where( { "name": "murdock", "level": 1, "registered": null } );

		@param String Field name
		@param String Where value
		@param String Operator
		@return Boolean
	*/
	function Where( whereProp, whereValue = "", operator = null )
	{
		// Processing for 1 parameter, this can either be a string or table
		if ( whereValue == "" && operator == null )
		{
			// If a string is passed, process it as a raw statement
			if ( typeof( whereProp ) == "string" )
			{
				this._where.append( whereProp );
				return true;
			}
			
			// Process tables as fieldname => value
			if ( typeof( whereProp ) == "table" )
			{
				foreach( field, value in whereProp )
					this._where.append( this._format( field, value, "=" ) );
				
				return true;
			}
			
			// Wrong parameter type passed, return false
			return false;
		}
		
		// Process field and value parameter
		if ( whereValue != "" )
		{
			this._where.append( this._format( whereProp, whereValue, ( operator != null ? operator : "=" ) ) );
			return true;
		}
		
		// Wrong amount of parameters passed, return false
		return false;
	}
	
	/**
		This method adds an order by statement to the query.
		
		@param String Field name
		@param String Orderby direction
		@return Boolean
	*/
	function OrderBy( orderByField, orderbyDirection = "DESC" )
	{
		// Convert direction to uppercase to compare and build a proper query
		orderbyDirection = orderbyDirection.toupper();
		
		// Check legal directions
		if ( [ "DESC", "ASC" ].find( orderbyDirection ) == null )
			return false;
		
		// Exception for RAND()
		local orderByFieldu = orderByField.toupper();
		if ( orderByFieldu == "RAND()" )
			this._orderBy.append( orderByFieldu );
		else
			this._orderBy.append( ::format( "%s %s", orderByField, orderbyDirection ) );
		
		return true;
	}
	
	/**
		This method adds a join statement to the query.
		
		Example:
			q.Join( "myTable", "field1 <> field2", "LEFT" );
		
		@param String Table name
		@param String Join condition
		@param String Join type
		@return Boolean
	*/
	function Join( joinTable, joinCondition, joinType = null )
	{
		local allowedTypes = [ "LEFT", "RIGHT", "OUTER", "INNER", "LEFT OUTER", "RIGHT OUTER" ];
		
		// Convert join type to uppercase to compare and build a proper query
		if ( joinType != null )
			joinType = joinType.toupper();
		
		// Check legal join types
		if ( joinType != null && allowedTypes.find( joinType ) == null )
			return false;
		
		this._join.append( ::format( "%sJOIN %s ON %s",
									( joinType != null ? joinType + " " : "" ),
									joinTable,
									joinCondition ) );
		return true;
	}

	/**
		Method used to update values of specific fields on a duplicate key entry in INSERT queries.
		Accepts raw string, table or 2 arguments as fieldname and value.
		
		@param String Field name
		@param String Where value
		@return Boolean
	*/
	function Duplicate( duplicateProp, newValue = "" )
	{
		// Processing for 1 parameter, this can either be a string or table
		if ( newValue == "" )
		{
			// If a string is passed, process it as a raw statement
			if ( typeof( duplicateProp ) == "string" )
			{
				this._duplicate.append( duplicateProp );
				return true;
			}
			
			// Process tables as fieldname => value
			if ( typeof( duplicateProp ) == "table" )
			{
				foreach( field, value in duplicateProp )
					this._duplicate.append( this._formatUpdate( field, value ) );
				
				return true;
			}
			
			// Wrong parameter type passed, return false
			return false;
		}
		
		// Process field and value parameter
		if ( newValue != "" )
		{
			this._duplicate.append( this._formatUpdate( duplicateProp, newValue ) );
			return true;
		}
		
		// Wrong amount of parameters passed, return false
		return false;
	}
	
	function _buildUpdate()
	{
		// Build UPDATE query
		local updateLen = this._update.len();
		if ( updateLen > 0 )
		{
			local _updateidx = 0;
			foreach( field, val in this._update )
			{
				this._query += ::format( "%s%s%s",  ( _updateidx == 0 ? " SET " : "" ),
													this._formatUpdate( field, val ),
													( _updateidx != (updateLen - 1) ? ", " : "" ) );
				_updateidx++;
			}
		}
		else
		{
			updateLen = this._updatestr.len();
			if ( updateLen > 0 )
				this._query += ::format( " SET %s", this._updatestr );
		}
	}
	
	function _buildInsert()
	{
		// Build INSERT query
		local insertLen = this._insert.len();
		if ( insertLen > 0 )
		{
			local queryEnd = "";
			local fields = "(", values = "(";
			local _insertidx = 0;
			
			foreach( field, val in this._insert )
			{
				// Continue the list of fields/values with comma or end it with a bracket
				queryEnd = ( _insertidx++ != (insertLen - 1) ? ", " : ")" );
				
				fields += ::format( "%s%s", field, queryEnd );
				values += ::format( "%s%s", this._formatInsert( val ), queryEnd );	
			}
			
			this._query += ::format( " %s VALUES %s", fields, values );
		}
	}
	
	function _buildDuplicate()
	{
		// Build ON DUPLICATE KEY statement
		local duplicateLen = this._duplicate.len();
		if ( duplicateLen > 0 )
		{
			foreach( idx, cond in this._duplicate )
			{
				this._query += ::format( "%s%s%s",  ( idx == 0 ? " ON DUPLICATE KEY UPDATE " : "" ),
													cond,
													( idx != (duplicateLen - 1) ? ", " : "" ) );
			}
		}
	}
	
	function _buildJoin()
	{
		// Build JOIN statements
		local joinLen = this._join.len();
		if ( joinLen > 0 )
		{
			foreach( idx, cond in this._join )
				this._query += " " + cond;
		}
	}
	
	function _buildWhere()
	{
		// Build WHERE clauses
		local whereLen = this._where.len();
		if ( whereLen > 0 )
		{
			foreach( idx, cond in this._where )
			{
				this._query += ::format( "%s%s%s",  ( idx == 0 ? " WHERE " : "" ),
													cond,
													( idx != (whereLen - 1) ? " AND " : "" ) );
			}
		}
	}
	
	function _buildOrderBy()
	{
		// Build ORDER BY clauses
		local orderByLen = this._orderBy.len();
		if ( orderByLen > 0 )
		{
			foreach( idx, field in this._orderBy )
			{
				this._query += ::format( "%s%s%s",  ( idx == 0 ? " ORDER BY " : "" ),
													field,
													( idx != (orderByLen - 1) ? ", " : "" ) );
			}
		}
	}
	
	function _buildLimit()
	{
		// Build LIMIT
		if ( this._limit != null )
			this._query += ::format( " LIMIT %d", this._limit );
	}
	
	function _buildQuery()
	{
		this._buildUpdate();
		
		this._buildInsert();
		
		this._buildDuplicate();
		
		this._buildJoin();
		
		this._buildWhere();
		
		this._buildOrderBy();
		
		this._buildLimit();
	}
	
	/**
		Execute the query and return an array with returned data.
		First parameter should be the table name.
		Specify the amount of rows returned in the second parameter, use null for all rows.
		Lastly, you can specify which columns to return (other than "*" by default) in the 3rd parameter.
		
		Examples:
			Get( "usersTable" );
			Get( "usersTable", 10 );
			Get( "usersTable", null, "username, email" );
		
		@param String Table name
		@param Integer Number of rows to return
		@param String Columns to select
		@return Array
	*/
	function Get( szTable, iNumRows = null, szColumns = '*' )
	{
		// Return null when no valid tablename is given
		if ( typeof( szTable ) != "string" )
			return null;
		
		// Specify table to query
		this._table = szTable;
		
		// Add limit to the query if specified
		if ( typeof( iNumRows ) == "integer" )
			this._limit = iNumRows;
		else
			this._limit = null;
		
		// Add columns to query if specified
		if ( typeof( szColumns ) == "string" )
			this._columns = szColumns;
		else
			this._columns = "*";
		
		// Build up the query
		this._query = ::format( "SELECT %s FROM %s", this._columns, this._table );
		this._buildQuery();
		
		// Query database
		local rows = [], row = null;
		local res = ::mysql_query( this._pDatabase.Instance, this._query );
		
		while( row = ::mysql_fetch_assoc( res ) )
			rows.append( row );
		
		// Free memory and return data array
		::mysql_free_result( res );
		return rows;
	}
	
	/**
		Execute the query and return an array with returned data.
		The first parameter should be the table name.
		Optionally you can specify a list of columns returned.
		
		Examples:
			GetOne( "usersTable" );
			GetOne( "usersTable", "username, email" );
		
		@param String Table name
		@param String Columns to select
		@return Array
	*/
	function GetOne( szTable, szColumns = "*" )
	{
		local res = this.Get( szTable, 1, szColumns );
		return ( res.len() > 0 ? res[0] : [] );
	}
	
	/**
		Execute a delete query. Don't forget to add where clauses first.
		
		@param String Table name
		@param Integer Number of rows (optional)
		@return Query result
	*/
	function Delete( szTable, iNumRows = null )
	{
		// Add limit to the query if specified
		if ( typeof( iNumRows ) == "integer" )
			this._limit = iNumRows;
		else
			this._limit = null;
		
		// Build up the query
		this._query = ::format( "DELETE FROM %s", szTable );
		this._buildQuery();
		
		// Query database
		return this._pDatabase.Query( this._query );
	}
	
	/**
		Method to update data in a table.
		Second parameter should be a table of field=>value items.
		
		@param String Table name
		@param Table|String Table data or raw string
		@return Query result
	*/
	function Update( szTable, updateData )
	{
		if ( typeof( szTable ) != "string" )
			return false;
		
		if ( typeof( updateData ) == "table" )
			this._update = updateData;
		else if ( typeof( updateData ) == "string" )
			this._updatestr = updateData;
		else
			return false;
		
		// Build up the query
		this._query = ::format( "UPDATE %s", szTable );
		this._buildQuery();
		
		// Query database
		return this._pDatabase.Query( this._query );
	}
	
	/**
		Method to insert data in a table.
		Second parameter should be a table of field=>value items.
		
		@param String Table name
		@param Table Insert data
		@return Query result
	*/
	function Insert( szTable, aInsertData )
	{
		if ( typeof( szTable ) != "string" || typeof( aInsertData ) != "table" )
			return false;
		
		this._insert = aInsertData;
		
		// Build up the query
		this._query = ::format( "INSERT INTO %s", szTable );
		this._buildQuery();
		
		// Query database
		return this._pDatabase.Query( this._query );
	}
	
	/**
		Method to insert data in a table, without waiting for server response.
		Second parameter should be a table of field=>value items.
		Note this only works on MyISAM, MEMORY, ARCHIVE engine tables.
		
		@param String Table name
		@param Table Insert data
		@return Query result
	*/
	function InsertDelayed( szTable, aInsertData )
	{
		if ( typeof( szTable ) != "string" || typeof( aInsertData ) != "table" )
			return false;
		
		this._insert = aInsertData;
		
		// Build up the query
		this._query = ::format( "INSERT DELAYED INTO %s", szTable );
		this._buildQuery();
		
		// Query database
		return this._pDatabase.Query( this._query );
	}
	
	function GetQuery( bBuild = false )
	{
		if ( bBuild )
			this._buildQuery();
		
		return this._query;
	}
	
	function Clear()
	{
		this._query	= "";
		this._table = "";
		this._columns = "*";
		this._limit = null;
		this._updatestr = "";
		
		this._where.clear();
		this._orderBy.clear();
		this._join.clear();
		this._duplicate.clear();
		this._update.clear();
		this._insert.clear();
	}
};
